#!/bin/bash

#SBATCH --job-name=tfexample
#SBATCH --mail-user=
#SBATCH --mail-type=END,FAIL
#SBATCH --mem=16G
#SBATCH --time=03:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --output=tfexample-%J.stdout
#SBATCH --error=tfexample-%J.stderr

# Specify physical location of sif file
mysif=tensorflow-dcc.sif

# Check nvidia card and driver
singularity exec --nv ${mysif} nvidia-smi -q


# Specify directory containing yoru code (edit the line below)
mycodedir=[PUT absolute path to directory containing example.py from the repo]

# Execute python script
singularity exec --nv \
  --bind ${mycodedir}:/mnt/code/:ro \
  ${mysif} \
  /opt/tensorflow/bin/python3 /mnt/code/example.py

