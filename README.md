# Tensorflow DCC

## Download sif file
```
$ curl -O https://research-singularity-registry.oit.duke.edu/kowzar/tensorflow-dcc.sif
```

## Start interactive shell on a gpu node
```
srun --partition gpu-common --gres=gpu --pty bash -i
```


## Check nvidia card and driver 
```
$ singularity exec --nv tensorflow-dcc.sif nvidia-smi -q
```

## Start python shell
```
$ singularity exec --nv tensorflow-dcc.sif /opt/tensorflow/bin/python3
```


## Execute python script
```
$ singularity exec --nv tensorflow-dcc.sif /opt/tensorflow/bin/python3 example.py
```


## Start jupyter
```
$ singularity exec  --nv tensorflow-dcc.sif /opt/tensorflow/bin/jupyter-notebook
```

## Submit SLURM batch job
Edit example.q before running this

```
$ sbatch --partition gpu-common --gres=gpu example.q
```

